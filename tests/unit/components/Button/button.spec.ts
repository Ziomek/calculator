import { shallowMount } from '@vue/test-utils';
import Button from '@/components/Button/Button.vue';

describe('Button.vue', () => {
  it('renders props.title when passed', () => {
    const title = 'button';
    const wrapper = shallowMount(Button, {
      propsData: { title },
    });
    expect(wrapper.text()).toMatch(title);
    const buttonStyle = wrapper.find('button').attributes().style;
    expect(buttonStyle).toContain('background: white');
    expect(buttonStyle).toContain('border-radius: 0 0 0 0;');
    expect(buttonStyle).toContain('width: 70px');
  });

  it('Check if backgournd color is lightgray when pass color props', () => {
    const color = 'lightgray';
    const title = 'button';
    const wrapper = shallowMount(Button, {
      propsData: { title, color },
    });
    const buttonStyle = wrapper.find('button').attributes().style;
    expect(buttonStyle).toContain('background: lightgray');
  });

  it('Check if width is 140px when pass big props', () => {
    const big = true;
    const title = 'button';
    const wrapper = shallowMount(Button, {
      propsData: { title, big },
    });
    const buttonStyle = wrapper.find('button').attributes().style;
    expect(buttonStyle).toContain('width: 140px');
  });

  it('Check if fontColor is set orange when pass fontColor props', () => {
    const fontColor = 'orange';
    const title = 'button';
    const wrapper = shallowMount(Button, {
      propsData: { title, fontColor },
    });
    const buttonStyle = wrapper.find('button').attributes().style;
    expect(buttonStyle).toContain('color: orange');
  });

  it('Check if borderRadius is set on `0 0 0 4px` when pass borderRadius props', () => {
    const borderRadius = '0 0 0 4px';
    const title = 'button';
    const wrapper = shallowMount(Button, {
      propsData: { title, borderRadius },
    });
    const buttonStyle = wrapper.find('button').attributes().style;
    expect(buttonStyle).toContain('border-radius: 0 0 0 4px');
  });

  it('Check if emit event is rise after click on button', async () => {
    const title = 'button';
    const wrapper = shallowMount(Button, {
      propsData: { title },
    });
    const button = wrapper.find('button');
    await button.trigger('click');
    expect(wrapper.emitted().click).toBeTruthy();
  });
});
