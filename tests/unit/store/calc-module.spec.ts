import { calc } from '@/store/modules';
import { createMockStore } from '../../utils/createMockStore';

describe('calc class', () => {
  let state = createMockStore({});

  const defaultEquiantion = '2 + 2';
  const defaultResult = 4;
  const defaultCalcAction = [
    {
      value: 2,
      action: '+',
    },
    {
      value: 2,
      action: '+',
    },
  ];

  beforeEach(() => {
    state = {
      equation: defaultEquiantion,
      calcActions: defaultCalcAction,
      result: defaultResult,
    };
  });

  describe('getters', () => {
    describe('get equation', () => {
      it('should return the defaultEquiantion from getEquation', () => {
        const returnedEquiantion = calc.getters.getEquation(state);
        expect(returnedEquiantion).toEqual(defaultEquiantion);
      });
    });

    describe('get result', () => {
      it('should return the defaultResult from getResult', () => {
        const returnedResult = calc.getters.getResult(state);
        expect(returnedResult).toEqual(defaultResult);
      });
    });

    describe('get equation', () => {
      it('should return the defaultCalcAction from getCalcActions', () => {
        const returnedCalActions = calc.getters.getCalcActions(state);
        expect(returnedCalActions).toEqual(defaultCalcAction);
      });
    });
  });

  describe('mutations', () => {
    describe('CLEAR', () => {
      it('should clear the calc', () => {
        calc.mutations.CLEAR(state);
        expect(state.equation).toEqual('0');
        expect(state.result).toEqual(0);
        expect(state.calcActions).toEqual([]);
      });
    });

    describe('SET_ACTION ', () => {
      it('should setAction', () => {
        calc.mutations.SET_ACTION(state, { value: 10, action: '+' });
        expect(state.calcActions.length).toEqual(3);
        expect(state.calcActions[2]).toEqual({ value: 10, action: '+' });
      });
    });

    describe('SET_EQUATION ', () => {
      it('should set equation', () => {
        calc.mutations.SET_EQUATION(state, ' + 12');
        expect(state.equation).toEqual('2 + 2 + 12');
      });

      it('should replace lase element', () => {
        state.equation = '2 +';
        calc.mutations.SET_EQUATION(state, '/');
        expect(state.equation).toEqual('2 /');
      });

      it('should set equation when default value', () => {
        state.equation = '0';
        calc.mutations.SET_EQUATION(state, '2');
        expect(state.equation).toEqual('2');
      });
    });

    describe('UPDATE_ACTION ', () => {
      it('should replace lase element', () => {
        calc.mutations.UPDATE_ACTION(state, '/');
        expect(state.calcActions[2].action).toEqual('/');
      });

      it('CalcActions is empty should break', () => {
        state.calcActions = [];
        calc.mutations.UPDATE_ACTION(state, '/');
        expect(state.calcActions).toEqual([]);
      });
    });

    describe('CALCULATE ', () => {
      it('CalcActions is empty should break', () => {
        state.calcActions = [];
        calc.mutations.CALCULATE(state);
        expect(state.result).toEqual(defaultResult);
      });

      it('CalcActions has length 1 result is equal value', () => {
        state.calcActions = [{ value: 10, action: '+' }];
        calc.mutations.CALCULATE(state);
        expect(state.result).toEqual(10);
      });

      it('Calculate sum two value', () => {
        state.calcActions = [{ value: 2, action: '+' }, { value: 2, action: '+' }];
        calc.mutations.CALCULATE(state);
        expect(state.result).toEqual(4);
      });

      it('Calculate subtraction two value', () => {
        state.calcActions = [{ value: 5, action: '-' }, { value: 2, action: '+' }];
        calc.mutations.CALCULATE(state);
        expect(state.result).toEqual(3);
      });

      it('Calculate multiply two value', () => {
        state.calcActions = [{ value: 3, action: 'x' }, { value: 3, action: '+' }];
        calc.mutations.CALCULATE(state);
        expect(state.result).toEqual(9);
      });

      it('Calculate divide two value', () => {
        state.calcActions = [{ value: 6, action: '/' }, { value: 3, action: '+' }];
        calc.mutations.CALCULATE(state);
        expect(state.result).toEqual(2);
      });

      it('Calculate data with mathematical order', () => {
        state.calcActions = [{ value: 2, action: '+' }, { value: 2, action: 'x' }, { value: 2, action: '+' }];
        calc.mutations.CALCULATE(state);
        expect(state.result).toEqual(6);
      });

      it('Invalid action return 0', () => {
        state.calcActions = [{ value: 2, action: '*' }, { value: 2, action: '-' }];
        calc.mutations.CALCULATE(state);
        expect(state.result).toEqual(0);
      });

      it('Calculate sum two value with diffrent action', () => {
        state.calcActions = [{ value: 2, action: '+' }, { value: 2, action: 'x' }];
        calc.mutations.CALCULATE(state);
        expect(state.result).toEqual(4);
      });
    });
  });

  describe('actions', () => {
    const commit = jest.fn();

    it('setAction - should commit SET_ACTION', () => {
      const calAction = {
        value: 12,
        action: '+',
      };
      calc.actions.setAction({ commit }, calAction);
      expect(commit).toBeCalledWith('SET_ACTION', calAction);
    });

    it('clear - should commit CLEAR', () => {
      calc.actions.clear({ commit });
      expect(commit).toBeCalledWith('CLEAR');
    });

    it('setEquation - should commit SET_EQUATION', () => {
      const equation = '12';
      calc.actions.setEquation({ commit }, equation);
      expect(commit).toBeCalledWith('SET_EQUATION', equation);
    });

    it('updateAction - should commit UPDATE_ACTION', () => {
      const action = '+';
      calc.actions.updateAction({ commit }, action);
      expect(commit).toBeCalledWith('UPDATE_ACTION', action);
    });

    it('calculate - should commit CALCULATE', () => {
      calc.actions.calculate({ commit });
      expect(commit).toBeCalledWith('CALCULATE');
    });
  });
});
