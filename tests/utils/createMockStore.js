import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

/* istanbul ignore next */
export const createMockStore = (data) => {
  const input = data ?? {};
  return new Vuex.Store({
    modules: {
      calc: {
        namespaced: true,
        getters: {
          getResult: () => input.getResult || 0,
          getEquation: () => input.getEquation || '',
          getCalcActions: () => input.getCalcActions || [],
        },
        mutations: {
          SET_EQUATION: input.SET_EQUATION || jest.fn(),
          UPDATE_ACTION: input.UPDATE_ACTION || jest.fn(),
          SET_ACTION: input.SET_ACTION || jest.fn(),
          SET_FLIGHT_HISTORY_PANEL: input.SET_FLIGHT_HISTORY_PANEL || jest.fn(),
          CALCULATE: input.CALCULATE || jest.fn(),
          CLEAR: input.CLEAR || jest.fn(),
        },
        actions: {
          setEquation: input.setEquation || jest.fn(),
          setAction: input.setAction || jest.fn(),
          updateAction: input.updateAction || jest.fn(),
          calculate: input.calculate || jest.fn(),
          clear: input.clear || jest.fn(),
        },
      },
    },
  });
}
