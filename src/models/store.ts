import { CalculatorState } from './calculator';

export interface State {
  calc: CalculatorState[];
}
