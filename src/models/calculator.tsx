export interface CalcAction {
  value: number;
  action: string;
}

export interface CalculatorState {
  equation: string;
  calcActions: Array<CalcAction>;
  result: number;
}
