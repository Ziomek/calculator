import { State } from '@/models/store';
import Vue from 'vue';
import Vuex from 'vuex';
import { calc } from '@/store/modules';

Vue.use(Vuex);

export default new Vuex.Store<State>({
  modules: {
    calc,
  },
});
