import { ActionContext } from 'vuex';
import { CalcAction, CalculatorState } from '../../models/calculator';

const calcValue = (a: number, b: number, action: string): number => {
  switch (action) {
    case '+': {
      return a + b;
    }
    case '-': {
      return a - b;
    }
    case '/': {
      return a / b;
    }
    case 'x': {
      return a * b;
    }
    default: {
      return 0;
    }
  }
};

export default {
  namespaced: true,
  state: {
    equation: '0',
    calcActions: [],
    result: 0,
  } as CalculatorState,
  mutations: {
    SET_EQUATION(state: CalculatorState, data: string): void {
      const types = ['+', '-', '/', 'x'];
      if (types.includes(data) && types.includes(state.equation.slice(-1))) {
        state.equation = state.equation.slice(0, -1) + data;
      } else {
        state.equation = state.equation === '0' ? data : state.equation + data;
      }
    },
    UPDATE_ACTION(state: CalculatorState, data: string): void {
      if (state.calcActions.length === 0) {
        return;
      }
      const lastElement = state.calcActions[state.calcActions.length - 1];
      lastElement.action = data;
    },
    SET_ACTION(state: CalculatorState, data: CalcAction): void {
      state.calcActions.push(data);
    },
    CALCULATE(state: CalculatorState): void {
      let result = 0;
      if (state.calcActions.length === 0) {
        return;
      }
      if (state.calcActions.length === 1) {
        state.result = state.calcActions[0].value;
        return;
      }
      if (state.calcActions.length > 2) {
        for (let i = 0; i < state.calcActions.length; i += 1) {
          if (state.calcActions[i].action === 'x' || state.calcActions[i].action === '/') {
            if (i + 1 < state.calcActions.length) {
              state.calcActions[i].value = calcValue(state.calcActions[i].value,
                state.calcActions[i + 1].value, state.calcActions[i].action);
              state.calcActions[i].action = state.calcActions[i + 1].action;
              state.calcActions.splice(i + 1, 1);
            }
          }
        }
      }
      let currValue = state.calcActions[0].value;
      let currAction = state.calcActions[0].action;
      for (let i = 1; i < state.calcActions.length; i += 1) {
        const nextValue = state.calcActions[i].value;
        const nextAction = state.calcActions[i].action;
        result = calcValue(currValue, nextValue, currAction);
        currValue = result;
        currAction = nextAction;
      }
      state.result = result;
    },
    CLEAR(state: CalculatorState): void {
      state.calcActions = [];
      state.result = 0;
      state.equation = '0';
    },
  },
  actions: {
    setEquation(_: ActionContext<CalculatorState, CalculatorState>, data: string): void {
      _.commit('SET_EQUATION', data);
    },
    setAction(_: ActionContext<CalculatorState, CalculatorState>, data: CalcAction): void {
      _.commit('SET_ACTION', data);
    },
    updateAction(_: ActionContext<CalculatorState, CalculatorState>, data: string): void {
      _.commit('UPDATE_ACTION', data);
    },
    calculate(_: ActionContext<CalculatorState, CalculatorState>): void {
      _.commit('CALCULATE');
    },
    clear(_: ActionContext<CalculatorState, CalculatorState>): void {
      _.commit('CLEAR');
    },
  },
  getters: {
    getResult: (state: CalculatorState): number => state.result,
    getEquation: (state: CalculatorState): string => state.equation,
    getCalcActions: (state: CalculatorState): Array<CalcAction> => state.calcActions,
  },
};
